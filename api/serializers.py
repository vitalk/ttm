#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import date
from rest_framework import serializers

from .models import (
    User, Task, Project
)


class DynamicFieldsSerializer(serializers.ModelSerializer):
    """Extends the base ModelSerializer class to support dynamic excluding
    fields from serializer on init time::

        class UserSerializer(DynamicFieldsSerializer):
            class Meta:
                model = User

        class TaskSerializer(DynamicFieldsSerializer):
            assignee = UserSerializer(fields=('id',))

            class Meta:
                model = Task

    """
    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)

        super(DynamicFieldsSerializer, self).__init__(*args, **kwargs)

        if fields:
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field in (existing - allowed):
                self.fields.pop(field)


class UserSerializer(DynamicFieldsSerializer,
                     serializers.HyperlinkedModelSerializer):
    role = serializers.ChoiceField(
        (User.ROLE.MANAGER, User.ROLE.DEVELOPER),
        required=True
    )
    tasks = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='task-detail',
    )
    projects = serializers.PrimaryKeyRelatedField(
        many=True,
        read_only=True,
    )

    class Meta:
        model = User
        fields = (
            'url', 'username', 'email', 'role', 'tasks', 'projects'
        )
        read_only_fields = ('username',)


class UserListSerializer(UserSerializer):
    pass


class CreateUserSerializer(UserSerializer):

    class Meta:
        model = User
        fields = (
            'url', 'username', 'password', 'email', 'role',
            'tasks', 'projects'
        )
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, payload):
        # Without this the password will be stored
        # in plain text.
        user = User.objects.create_user(**payload)
        return user


class ProjectSerializer(DynamicFieldsSerializer,
                        serializers.HyperlinkedModelSerializer):
    name = serializers.CharField(
        required=False,
        allow_null=True,
        max_length=180
    )
    assignees = UserSerializer(
        read_only=True,
        many=True,
        fields=('url', 'username', 'role')
    )
    tasks = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name="task-detail"
    )

    class Meta:
        model = Project
        fields = (
            'url', 'name', 'assignees', 'tasks'
        )


class ProjectListSerializer(ProjectSerializer):
    pass


class TaskSerializer(DynamicFieldsSerializer,
                     serializers.HyperlinkedModelSerializer):
    title = serializers.CharField(
        max_length=100,
        required=True
    )
    description = serializers.CharField(
        required=True
    )
    due_date = serializers.DateField(
        required=True
    )
    assignee_id = serializers.PrimaryKeyRelatedField(
        write_only=True,
        required=False,
        allow_null=True,
        source='assignee',
        queryset=User.objects.all()
    )
    assignee = UserSerializer(
        read_only=True,
        many=False,
        fields=('url', 'username', 'email', 'role')
    )
    project_id = serializers.PrimaryKeyRelatedField(
        write_only=True,
        required=False,
        allow_null=True,
        source='project',
        queryset=Project.objects.all()
    )
    project = ProjectSerializer(
        read_only=True,
        fields=('url', 'name')
    )

    class Meta:
        model = Task
        fields = (
            'url', 'title', 'description', 'due_date',
            'assignee', 'assignee_id', 'project', 'project_id'
        )

    def validate_due_date(self, value):
        """Ensures that due date points to the future."""
        if value < date.today():
            raise serializers.ValidationError(
                'Invalid value, due date should points '
                'to the future.'
            )
        return value

    def create(self, payload):
        task = super(TaskSerializer, self).create(payload)
        task.change_assignee(payload.get('assignee'), force=True)
        return task

    def update(self, instance, payload):
        task = super(TaskSerializer, self).update(instance, payload)
        task.change_assignee(
            previous_assignee=instance.assignee,
            new_assignee=payload.get('assignee')
        )
        return task


class TaskListSerializer(TaskSerializer):
    project = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='project-detail'
    )
