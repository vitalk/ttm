#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
from actstream.managers import (
    ActionManager, stream
)


class TaskActionManager(ActionManager):

    @stream
    def mystream(self, obj, verb='assigned to', time=None):
        if time is None:
            time = datetime.now()
        return obj.actor_actions.filter(verb=verb, timestamp__lte=time)
