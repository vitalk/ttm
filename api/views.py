#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import (
    viewsets, mixins, status
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from .models import (
    User, Task, Project
)
from .permissions import (
    IsAuthenticatedOrIsCreation, IsReadOnlyOrIsManager,
    IsReadOnlyOrIsManagerOrIsOwner
)
from .serializers import (
    UserSerializer, UserListSerializer, CreateUserSerializer, TaskSerializer,
    TaskListSerializer, ProjectSerializer, ProjectListSerializer
)


class Collection(mixins.CreateModelMixin,
                 mixins.ListModelMixin,
                 viewsets.GenericViewSet):
    permission_classes = (
        IsAuthenticated, IsReadOnlyOrIsManager
    )


class Resource(mixins.RetrieveModelMixin,
               mixins.UpdateModelMixin,
               mixins.DestroyModelMixin,
               viewsets.GenericViewSet):
    permission_classes = (
        IsAuthenticated, IsReadOnlyOrIsManager
    )


class UserList(Collection):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserListSerializer
    permission_classes = (IsAuthenticatedOrIsCreation,)

    def create(self, request, *args, **kwargs):
        self.serializer_class = CreateUserSerializer
        return super(UserList, self).create(request, *args, **kwargs)


class UserResource(Resource):
    serializer_class = UserSerializer
    queryset = User.objects.all().order_by('-date_joined')
    permission_classes = (
        IsAuthenticated, IsReadOnlyOrIsManagerOrIsOwner,
    )


class CurrentUserResource(Resource):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request):
        serializer = self.get_serializer(request.user)
        return Response(serializer.data)


class TaskList(Collection):
    serializer_class = TaskListSerializer
    queryset = Task.objects.all().order_by('-due_date')


class TaskResource(Resource):
    queryset = Task.objects.all().order_by('-due_date')
    serializer_class = TaskSerializer


class ProjectResource(Resource):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class ProjectList(Collection):
    queryset = Project.objects.all()
    serializer_class = ProjectListSerializer


class NestedTaskList(Collection):
    queryset = Task.objects.all()
    serializer_class = TaskListSerializer

    def get_project(self, request, project_pk=None):
        return get_object_or_404(Project, pk=project_pk)

    def get_queryset(self):
        return Task.objects.filter(project=self.kwargs['project_pk'])

    def create(self, request, *args, **kwargs):
        project = self.get_project(request, project_pk=kwargs['project_pk'])
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        task = serializer.save()
        task.assign_project(project)
        task.save()
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
        )


class NestedTaskResource(Resource):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(project=self.kwargs['project_pk'])

    def destroy(self, request, *args, **kwargs):
        task = self.get_object()
        task.unassign_project()
        task.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
