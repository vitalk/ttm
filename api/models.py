#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from actstream import action


class Role(object):
    MANAGER = 1
    DEVELOPER = 2


class User(AbstractUser):
    ROLE = Role

    role = models.IntegerField(
       choices=(
           (ROLE.MANAGER, 'Manager'),
           (ROLE.DEVELOPER, 'Developer'),
       ),
       default=ROLE.MANAGER
    )


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Project(models.Model):
    name = models.CharField(
        max_length=180,
        null=True
    )
    assignees = models.ManyToManyField(
        User,
        related_name="projects"
    )

    def assign_user(self, user):
        self.assignees.add(user)

    def unassign_user(self, user):
        self.assignees.remove(user)


class Task(models.Model):
    title = models.CharField(
        max_length=100
    )
    description = models.TextField()
    due_date = models.DateField()
    assignee = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="tasks",
        blank=True,
        null=True
    )
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name="tasks",
        blank=True,
        null=True
    )

    def assign_user(self, user):
        """Method used to assign user to the current task."""
        self.change_assignee(user)

    def unassign_user(self):
        """Method used to unassign user from the current task."""
        self.change_assignee(None)

    def change_assignee(self, new_assignee, previous_assignee=None, force=True):
        if new_assignee is not None:
            new_assignee = User.objects.get(pk=new_assignee.id)

        if previous_assignee is None:
            previous_assignee = self.assignee
        if force or previous_assignee != new_assignee:
            self.assignee = new_assignee
            if new_assignee:
                action.send(
                    self, verb='assigned to', target=new_assignee
                )

    def assign_project(self, project):
        """Method used to assign task to specific project."""
        self.project = project

    def unassign_project(self):
        """Method used to unassign task from project."""
        self.project = None
