#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.apps import AppConfig


class ApiAppConfig(AppConfig):
    name = 'api'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Task'), self.get_model('User'))
