#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import permissions
from .models import User


class IsReadOnlyOrIsManager(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user.role == User.ROLE.MANAGER:
            return True


class IsReadOnlyOrIsManagerOrIsOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        elif request.user.role == User.ROLE.MANAGER:
            return True

        elif request.user == obj:
            return True


class IsAuthenticatedOrIsCreation(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated():
            return True

        if view.action == 'create':
            return True
