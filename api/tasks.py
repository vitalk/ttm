#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from datetime import (
    datetime, timedelta
)
from celery import shared_task
from actstream.models import Action
from django.template import (
    Template, Context
)
from django.core.mail import send_mail
from .models import (
    User, Task
)


@shared_task
def send_activity_report(user_id, assigned_tasks, title):
    if not assigned_tasks:
        return

    user = User.objects.get(pk=user_id)
    from_email = 'noreply@example.com'

    report_template = Template('''
    The {{ assigned_tasks|length }} tasks has been assigned to you:

    <ul>
      {% for task_id, task_name in assigned_tasks %}
        <li><a href="{% url 'task-detail' task_id %}">{{ task_name }}</a></li>
      {% endfor %}
    </ul>
    ''')
    report_body = report_template.render(
        Context({'assigned_tasks': assigned_tasks})
    )

    send_mail(
        title,
        report_body,
        from_email,
        recipient_list=[user.email],
        fail_silently=False,
    )


@shared_task
def get_activity_report_for_user(user_id, start_date, title):
    task_ids = Action.objects.filter(
        target_object_id=user_id,
        timestamp__gt=start_date,
    ).values_list('actor_object_id', flat=True)

    tasks_assigned = Task.objects.filter(
        pk__in=list(task_ids),
        assignee_id__exact=user_id
    ).values_list('pk', 'title')
    send_activity_report.delay(user_id, list(tasks_assigned), title)


def get_within_delta(string):
    parts = string.split()
    return timedelta(**{parts[1]: int(parts[0])})


@shared_task
def generate_activity_report(period, title):
    start_date = datetime.utcnow() - get_within_delta(period)
    qs = Action.objects.order_by('timestamp')
    qs = qs.filter(timestamp__gt=start_date)
    qs = qs.values_list('target_object_id')
    for user_id, in set(qs.all()):
        get_activity_report_for_user.delay(user_id, start_date, title)


@shared_task
def get_hourly_report():
    generate_activity_report.delay(
        period='60 minutes',
        title='Hourly activity report'
    )


@shared_task
def get_daily_report():
    generate_activity_report.delay(
        period='1 days',
        title='Daily activity report'
    )
