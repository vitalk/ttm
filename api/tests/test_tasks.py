#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from datetime import (
    datetime, timedelta
)
from django.core import mail

from .factories import TaskFactory
from ..tasks import (
    get_activity_report_for_user, get_within_delta, generate_activity_report
)


@pytest.fixture
def batch_data(alice, bob):
    for task in TaskFactory.build_batch(3):
        task.change_assignee(alice)
        task.save()

    for task in TaskFactory.build_batch(1):
        task.change_assignee(bob)
        task.save()


@pytest.mark.usefixtures('batch_data')
class TestTasks:

    def test_get_within_delta(self):
        assert get_within_delta('2 hours') == timedelta(hours=2)
        assert get_within_delta('60 minutes') == timedelta(minutes=60)

    @pytest.fixture
    def now(self):
        return datetime.utcnow()

    @pytest.fixture
    def hour_ago(self, now):
        return now - timedelta(hours=1)

    def test_get_activity_report_for_user(self, alice, bob, hour_ago):
        get_activity_report_for_user(
            user_id=alice.id,
            start_date=hour_ago,
            title='Hourly report'
        )
        assert mail.outbox
        assert len(mail.outbox) == 1

        report_mail = mail.outbox[0]
        assert report_mail.subject == 'Hourly report'
        assert report_mail.to == [alice.email]
        assert '3 tasks has been assigned to you' in report_mail.body

    def test_get_activity_report_respect_start_date(self, now, alice):
        get_activity_report_for_user(
            user_id=alice.id, start_date=now, title='Dummy report'
        )

        assert not mail.outbox

    def test_generate_activity_report(self, alice, bob):
        generate_activity_report('5 minutes', 'Dummy report')

        assert mail.outbox
        assert len(mail.outbox) == 2
