#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from rest_framework.test import APIClient
from rest_framework_jwt.settings import api_settings

from ..models import User
from .factories import (
    UserFactory, TaskFactory, ProjectFactory
)


def jwt(user):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    return jwt_encode_handler(payload)


@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
def ca(client, alice):
    client.credentials(HTTP_AUTHORIZATION='JWT {}'.format(jwt(alice)))
    return client


@pytest.fixture
def cb(client, bob):
    client.credentials(HTTP_AUTHORIZATION='JWT {}'.format(jwt(bob)))
    return client


@pytest.fixture
def user(db):
    return UserFactory()


@pytest.fixture
def alice(db):
    return UserFactory(username='alice', role=User.ROLE.MANAGER)


@pytest.fixture
def bob(db):
    return UserFactory(username='bob', role=User.ROLE.DEVELOPER)


@pytest.fixture
def task(db):
    return TaskFactory(assignee=None, project=None)


@pytest.fixture
def unassigned_task(db, task):
    return task


@pytest.fixture
def project(db):
    return ProjectFactory()
