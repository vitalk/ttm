#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
import factory
from django.urls import reverse
from ..models import User
from .conftest import jwt
from .factories import UserFactory


class TestUserCollection:

    @pytest.fixture
    def payload(self, db):
        return factory.build(
            dict,
            FACTORY_CLASS=UserFactory,
        )

    @pytest.fixture
    def uri(self):
        return reverse('user-list')

    def test_post(self, client, uri, payload):
        res = client.post(uri, payload)
        assert res.status_code == 201

    def test_ensure_the_password_is_set(self, client, uri, payload):
        res = client.post(uri, payload)
        assert res.status_code == 201

        user = User.objects.filter(username=payload['username']).first()
        assert user.check_password(payload['password'])

    def test_post_as_manager(self, ca, uri, payload):
        res = ca.post(uri, payload)
        assert res.status_code == 201

    def test_post_as_developer(self, cb, uri, payload):
        res = cb.post(uri, payload)
        assert res.status_code == 201

    @pytest.fixture(params=['username', 'role', 'password'])
    def malformed_payload(self, payload, request):
        del payload[request.param]
        return payload

    def test_malformed_payload(self, ca, uri, malformed_payload):
        res = ca.post(uri, malformed_payload)
        assert res.status_code == 400
        assert b'field is required' in res.content

    def test_invalid_role(self, ca, uri, payload):
        payload['role'] = 42
        res = ca.post(uri, payload)
        assert res.status_code == 400
        assert b'is not a valid choice' in res.content

    def test_get(self, client, uri, db):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_manager(self, ca, uri, db):
        UserFactory.create_batch(10)

        res = ca.get(uri)
        assert res.status_code == 200
        assert len(res.data) == (10 + 1)  # self

    def test_get_as_developer(self, cb, uri):
        res = cb.get(uri)
        assert res.status_code == 200


class TestUser:

    @pytest.fixture
    def uri(self, alice):
        return reverse('user-detail', kwargs={'pk': alice.id})

    @pytest.fixture
    def payload(self):
        return factory.build(
            dict,
            FACTORY_CLASS=UserFactory,
        )

    def test_get(self, client, uri, alice):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_manager(self, ca, uri, alice):
        res = ca.get(uri)
        assert res.status_code == 200

    def test_ensure_the_password_is_not_exposed(self, ca, uri):
        res = ca.get(uri)
        assert 'password' not in res.data

    def test_get_as_developer(self, cb, uri, alice):
        res = cb.get(uri)
        assert res.status_code == 200

    def test_get_self(self, cb, uri, bob):
        res = cb.get(reverse('user-detail', kwargs={'pk': bob.id}))
        assert res.status_code == 200

    def test_put(self, client, uri, alice, payload):
        res = client.put(uri, payload)
        assert res.status_code == 401

    def test_put_as_manager(self, ca, uri, alice, payload):
        res = ca.put(uri, payload)
        assert res.status_code == 200
        assert res.data['username'] == alice.username
        assert res.data['email'] == payload['email']
        assert res.data['role'] == payload['role']

    def test_put_as_developer(self, cb, uri, alice, payload):
        res = cb.put(uri, payload)
        assert res.status_code == 403

    def test_put_self(self, cb, bob, payload):
        res = cb.put(reverse('user-detail', kwargs={'pk': bob.id}), payload)
        assert res.status_code == 200

    def test_delete(self, client, uri, alice):
        res = client.delete(uri)
        assert res.status_code == 401

    def test_delete_as_manager(self, ca, uri):
        res = ca.delete(uri)
        assert res.status_code == 204

    def test_delete_as_developer(self, cb, uri, alice):
        res = cb.delete(uri)
        assert res.status_code == 403

    def test_delete_self(self, cb, bob):
        res = cb.delete(reverse('user-detail', kwargs={'pk': bob.id}))
        assert res.status_code == 204


class TestAuth:

    @pytest.fixture
    def uri(self):
        return reverse('obtain_jwt_token')

    @pytest.fixture
    def me(self, db):
        user = UserFactory(username='vital')
        user.set_password('letmein')
        user.save()
        return user

    def test_obtain_auth_token(self, client, uri, me):
        res = client.post(uri, {'username': 'vital', 'password': 'letmein'})
        assert res.status_code == 200
        assert res.data['token'] == jwt(me)


class TestCurrentUser:

    @pytest.fixture
    def uri(self):
        return reverse('me')

    def test_get_unauth(self, client, uri):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get(self, ca, uri):
        res = ca.get(uri)
        assert res.status_code == 200
        assert res.data['username'] == 'alice'
