#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
import factory
from datetime import date
from django.urls import reverse
from ..models import Task
from .factories import (
    ProjectFactory, TaskFactory
)


class TestProjectCollection:

    @pytest.fixture
    def payload(self, db):
        return factory.build(
            dict,
            FACTORY_CLASS=ProjectFactory,
            name='Getting things done',
        )

    @pytest.fixture
    def uri(self):
        return reverse('project-list')

    def test_post(self, client, uri, payload):
        res = client.post(uri, payload)
        assert res.status_code == 401

    def test_post_as_manager(self, ca, uri, payload):
        res = ca.post(uri, payload)
        assert res.status_code == 201
        assert res.data['name'] == 'Getting things done'

    def test_post_as_developer(self, cb, uri, payload):
        res = cb.post(uri, payload)
        assert res.status_code == 403

    def test_get(self, client, uri):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_developer(self, cb, uri):
        res = cb.get(uri)
        assert res.status_code == 200

    def test_get_as_manager(self, ca, uri, db):
        ProjectFactory.create_batch(10)

        res = ca.get(uri)
        assert res.status_code == 200
        assert len(res.data) == 10


class TestProject:

    @pytest.fixture
    def payload(self, alice):
        return factory.build(
            dict,
            FACTORY_CLASS=ProjectFactory,
            name='Getting things done',
        )

    @pytest.fixture
    def uri(self, project):
        return reverse('project-detail', kwargs={'pk': project.id})

    def test_get(self, client, uri, project):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_manager(self, ca, uri, project):
        res = ca.get(uri)
        assert res.status_code == 200

    def test_get_as_developer(self, cb, uri, project):
        res = cb.get(uri)
        assert res.status_code == 200

    def test_put(self, client, uri, project, payload):
        res = client.put(uri, payload)
        assert res.status_code == 401

    def test_put_as_manager(self, ca, uri, project, payload):
        res = ca.put(uri, payload)
        assert res.status_code == 200

        project.refresh_from_db()
        assert project.name == 'Getting things done'

    def test_put_as_developer(self, cb, uri, project, payload):
        res = cb.put(uri, payload)
        assert res.status_code == 403

    def test_delete(self, client, uri):
        res = client.delete(uri)
        assert res.status_code == 401

    def test_delete_as_manager(self, ca, uri):
        res = ca.delete(uri)
        assert res.status_code == 204

    def test_delete_as_developer(self, cb, uri):
        res = cb.delete(uri)
        assert res.status_code == 403


class TestProjectTaskCollection:

    @pytest.fixture
    def payload(self, db):
        return factory.build(
            dict,
            FACTORY_CLASS=TaskFactory,
            due_date=date.today().strftime('%Y-%m-%d'),
            assignee=None,
            project=None,
        )

    @pytest.fixture
    def uri(self, project):
        return reverse('project-task-list', kwargs={'project_pk': project.id})

    def test_post(self, client, project, uri, payload):
        """Ensure the task is assigned to project."""
        res = client.post(uri, payload)
        assert res.status_code == 401

    def test_post_as_manager(self, ca, project, uri, payload):
        """Ensure the task is assigned to project."""
        res = ca.post(uri, payload)
        assert res.status_code == 201

        task = Task.objects.filter(project=project.id).first()
        assert task
        assert task.project == project

    def test_post_as_developer(self, cb, project, uri, payload):
        """Ensure the task is assigned to project."""
        res = cb.post(uri, payload)
        assert res.status_code == 403

    def test_get(self, client, project, uri):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_manager(self, ca, project, uri):
        TaskFactory.create_batch(
            10,
            project=project
        )

        res = ca.get(uri)
        assert len(res.data) == 10

    def test_get_as_developer(self, cb, project, uri):
        res = cb.get(uri)
        assert res.status_code == 200


class TestProjectTask:

    @pytest.fixture
    def project_task(self, project, task):
        task.assign_project(project)
        task.save()
        return task

    @pytest.fixture
    def uri(self, project, project_task):
        return reverse(
            'project-task-detail',
            kwargs={'project_pk': project.id, 'pk': project_task.id}
        )

    def test_get(self, client, uri):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_manager(self, ca, uri):
        res = ca.get(uri)
        assert res.status_code == 200

    def test_get_as_developer(self, cb, uri):
        res = cb.get(uri)
        assert res.status_code == 200

    def test_delete(self, client, uri, task, project):
        res = client.delete(uri)
        assert res.status_code == 401

    def test_delete_as_manager(self, ca, uri, task, project):
        """Ensure the task removed from project."""
        res = ca.delete(uri)
        assert res.status_code == 204

        task.refresh_from_db()
        assert task not in project.tasks.all()

    def test_delete_as_developer(self, cb, uri, task, project):
        res = cb.delete(uri)
        assert res.status_code == 403
