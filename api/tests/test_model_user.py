#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..models import User
from .factories import UserFactory


class TestUser:

    def test_model(self, alice):
        assert alice.username == 'alice'
        assert not alice.tasks.all()
        assert not alice.projects.all()

    def test_manager(self, alice):
        assert alice.role == User.ROLE.MANAGER

    def test_developer(self, bob):
        assert bob.role == User.ROLE.DEVELOPER

    def test_new_user_get_auth_token(self, db):
        user = UserFactory()

        assert user.auth_token is not None
