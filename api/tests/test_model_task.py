#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from unittest.mock import patch


class TestTask:

    def test_model(self, task, alice):
        assert task.title
        assert task.description
        assert task.due_date
        assert not task.assignee
        assert not task.project

    @patch('actstream.signals.action.send')
    def test_change_assignee(self, mock, task, alice, bob):
        assert task.assignee is None

        task.change_assignee(bob)
        task.save()

        assert task.assignee == bob
        assert mock.called
        assert mock.called_with(task, verb='assigned to', target=bob)

        task.change_assignee(alice)
        task.save()

        assert task.assignee == alice
        assert mock.called_with(task, verb='assigned to', target=alice)

        task.change_assignee(None)
        task.save()

        assert task.assignee is None
        assert mock.not_called

    def test_assign_project(self, task, project):
        assert task not in project.tasks.all()

        task.assign_project(project)
        task.save()
        assert task.project == project
        assert task in project.tasks.all()

    @pytest.fixture
    def project_task(self, task, project):
        task.assign_project(project)
        task.save()
        return task

    def test_unassign_project(self, project_task, project):
        project_task.unassign_project()
        project_task.save()

        assert project_task not in project.tasks.all()
