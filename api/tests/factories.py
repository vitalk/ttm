#!/usr/bin/env python
# -*- coding: utf-8 -*-
import factory

from ..models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'api.User'
        django_get_or_create = ('username',)

    id = factory.Sequence(lambda n: n)
    username = factory.Faker('user_name')
    email = factory.Faker('email')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    password = factory.Faker('password', length=10)
    role = factory.Iterator([User.ROLE.MANAGER, User.ROLE.DEVELOPER])
    is_active = True
    is_staff = False


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'api.Project'
        django_get_or_create = ('name',)

    id = factory.Sequence(lambda n: n)
    name = factory.Sequence(lambda n: 'project_{}'.format(n))

    @factory.post_generation
    def tasks(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for task in extracted:
                self.tasks.add(task)

    @factory.post_generation
    def assignees(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for user in extracted:
                self.assign_user(user)


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'api.Task'
        django_get_or_create = ('title',)

    id = factory.Sequence(lambda n: n)
    title = factory.Sequence(lambda n: 'title_{}'.format(n))
    description = factory.Sequence(lambda n: 'description_{}'.format(n))
    due_date = factory.Faker('date_time_this_month')
    assignee = factory.SubFactory(UserFactory)
    project = factory.SubFactory(ProjectFactory)
