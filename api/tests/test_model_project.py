#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest


class TestProject:

    def test_model(self, project):
        assert project.name
        assert not project.tasks.all()

    def test_assign_user(self, alice, project):
        project.assign_user(alice)
        project.save()

        assert alice in project.assignees.all()
        assert project in alice.projects.all()

    def test_multiple_assignments(self, alice, bob, project):
        project.assign_user(alice)
        project.assign_user(bob)
        project.save()

        assert list(project.assignees.all()) == [alice, bob]

    @pytest.fixture
    def assigned_project(self, alice, project):
        project.assign_user(alice)
        project.save()
        return project

    def test_unassign_user(self, alice, assigned_project):
        assigned_project.unassign_user(alice)
        project = assigned_project
        project.save()

        assert alice not in project.assignees.all()
        assert project not in alice.projects.all()
