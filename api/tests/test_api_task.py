#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import date
from unittest.mock import patch
import pytest
import factory
from django.urls import reverse
from .factories import TaskFactory
from ..models import Task


class TestTaskCollection:

    @pytest.fixture
    def payload(self, db):
        return factory.build(
            dict,
            FACTORY_CLASS=TaskFactory,
            assignee=None,
            project=None,
            due_date=date.today()
        )

    @pytest.fixture
    def uri(self):
        return reverse('task-list')

    def test_post(self, client, uri, payload):
        res = client.post(uri, payload)
        assert res.status_code == 401

    def test_post_as_manager(self, ca, uri, payload):
        res = ca.post(uri, payload)
        assert res.status_code == 201

    @patch('actstream.signals.action.send')
    def test_assign_user(self, mock, ca, uri, payload, alice):
        payload['assignee_id'] = alice.id
        res = ca.post(uri, payload)
        assert res.status_code == 201

        task = Task.objects.filter(assignee=alice).first()
        assert mock.called
        assert mock.called_with(task, verb='assigned to', target=alice)

    def test_post_as_developer(self, cb, uri, payload):
        res = cb.post(uri, payload)
        assert res.status_code == 403

    @pytest.fixture(params=['title', 'description', 'due_date'])
    def malformed_payload(self, payload, request):
        del payload[request.param]
        return payload

    def test_malformed_payload(self, ca, uri, malformed_payload):
        res = ca.post(uri, malformed_payload)
        assert res.status_code == 400
        assert b'field is required' in res.content

    def test_due_date_in_past(self, ca, uri, payload):
        payload['due_date'] = '2000-01-01'

        res = ca.post(uri, payload)
        assert res.status_code == 400
        assert b'due date should points to the future' in res.content

    def test_get(self, client, uri,):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_manager(self, ca, uri, db):
        TaskFactory.create_batch(10)

        res = ca.get(uri)
        assert res.status_code == 200
        assert len(res.data) == 10

    def test_get_as_developer(self, cb, uri):
        res = cb.get(uri)
        assert res.status_code == 200


class TestTask:

    @pytest.fixture
    def payload(self):
        return factory.build(
            dict,
            FACTORY_CLASS=TaskFactory,
            title='new title',
            description='new description',
            due_date=date.today().strftime('%Y-%m-%d'),
            assignee=None,
            project=None
        )

    @pytest.fixture
    def uri(self, task):
        return reverse('task-detail', kwargs={'pk': task.id})

    def test_get(self, client, task, uri):
        res = client.get(uri)
        assert res.status_code == 401

    def test_get_as_manager(self, ca, task, uri):
        res = ca.get(uri)
        assert res.status_code == 200

    def test_get_as_developer(self, cb, task, uri):
        res = cb.get(uri)
        assert res.status_code == 200

    def test_put(self, client, task, uri, payload):
        res = client.put(uri, payload)
        assert res.status_code == 401

    def test_put_as_manager(self, ca, task, uri, payload):
        res = ca.put(uri, payload)
        assert res.status_code == 200
        assert res.data['title'] == 'new title'
        assert res.data['description'] == 'new description'

    def test_put_as_developer(self, cb, task, uri, payload):
        res = cb.put(uri, payload)
        assert res.status_code == 403

    @patch('actstream.signals.action.send')
    def test_assign_user(self, mock, ca, alice, task, uri, payload):
        assert not task.assignee

        payload['assignee_id'] = alice.id
        res = ca.put(uri, payload)
        assert res.status_code == 200
        assert res.data['assignee']['username'] == 'alice'

        task.refresh_from_db()
        assert task.assignee

        assert mock.called
        assert mock.called_with(task, verb='assigned to', target=alice)

    @patch('actstream.signals.action.send')
    def test_unassign_user(self, mock, ca, alice, task, uri, payload):
        task.assign_user(alice)
        task.save()

        payload['assignee_id'] = None
        res = ca.put(uri, payload)
        assert res.status_code == 200
        assert res.data['assignee'] is None

        task.refresh_from_db()
        assert not task.assignee

        assert mock.not_called

    @patch('actstream.signals.action.send')
    def test_reassign_user(self, mock, ca, alice, bob, task, uri, payload):
        task.assign_user(alice)
        task.save()

        payload['assignee_id'] = bob.id
        res = ca.put(uri, payload)
        assert res.status_code == 200

        task.refresh_from_db()
        assert task.assignee == bob

        assert mock.called
        assert mock.called_with(task, verb='assigned to', target=bob)

    def test_assign_project(self, ca, task, project, uri, payload):
        assert not task.project

        payload['project_id'] = project.id
        res = ca.put(uri, payload)
        assert res.status_code == 200
        assert res.data['project']

        task.refresh_from_db()
        assert task.project

    def test_unassign_project(self, ca, task, project, uri, payload):
        task.assign_project(project)
        task.save()

        payload['project_id'] = None
        res = ca.put(uri, payload)
        assert res.status_code == 200
        assert res.data['project'] is None

        task.refresh_from_db()
        assert not task.project

    def test_delete(self, client, task, uri):
        res = client.delete(uri)
        assert res.status_code == 401

    def test_delete_as_manager(self, ca, task, uri):
        res = ca.delete(uri)
        assert res.status_code == 204

    def test_delete_as_developer(self, cb, task, uri):
        res = cb.delete(uri)
        assert res.status_code == 403
