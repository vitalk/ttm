#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Config for local development.
"""
from .base import *  # noqa


JWT_VERIFY_EXPIRATION = False

# In development, all tasks will be executed locally by blocking until the
# task returns
#CELERY_ALWAYS_EAGER = True

EMAIL_PORT = 1025
EMAIL_HOST = 'localhost'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
