#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test settings.
"""
from .base import *  # noqa


DEBUG = False

SECRET_KEY = 'wow such doge'

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

REST_FRAMEWORK['TEST_REQUEST_DEFAULT_FORMAT'] = 'json'

CELERY_ALWAYS_EAGER = True
