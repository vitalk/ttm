# Tiny task manager

Build on top of Django and Django REST Framework.

## Preflight check

```sh
pip install -r requirements/base.txt
```

```sh
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

## Testing

Run tests with py.test

```sh
pip install -r requirements/test.txt
```

```sh
py.test
```

## Celery

Run a celery worker

```sh
sudo rabbitmq-server
```

```sh
celery -A taskapp worker -l debug
```

```sh
celery -A taskapp beat
```

## REST API

### Authentication

All API requests require authentication via JWT.

```sh
http post :8000/api/v1/auth/jwt/ username=vital password=letmeinplease
```

```sh
{
    "token": "<token>"
}
```

### Resources

All available resources support CRUD operations.

Users
-----

- List users

    ```sh
    http get :8000/api/v1/users/ "Authorization: JWT <token>"
    ```

- Get user

    ```sh
    http get :8000/api/v1/users/1/ "Authorization: JWT <token>"
    ```

- Get current user

    ```sh
    http get :8000/api/v1/users/me/ "Authorization: JWT <token>"
    ```

- Create user

    ```sh
    http post :8000/api/v1/users/ \
        username=alice email=alice@example.com password=easytoremember role=1 \
        "Authorization: JWT <token>"
    ```

- Delete user

    ```sh
    http delete :8000/api/v1/users/1/ "Authorization: JWT <token>"
    ```

Tasks
-----

- List tasks

    ```sh
    http get :8000/api/v1/tasks/ "Authorization: JWT <token>"
    ```

- Create task

    ```sh
    http post :8000/api/v1/tasks/ \
        title="New issue" description="Save the world!" due_date="2018-01-01" \
        assignee_id=1 project_id=42 \
        "Authorization: JWT <token>"
    ```

- Edit task

    ```sh
    http put :8000/api/v1/tasks/ \
        title="New issue" description="Save the world! (not today)" \
        due_date="2018-01-01" \
        "Authorization: JWT <token>"
    ```

- Get task

    ```sh
    http get :8000/api/v1/tasks/26/ "Authorization: JWT <token>"
    ```

- Delete task

    ```sh
    http delete :8000/api/v1/tasks/26/ "Authorization: JWT <token>"
    ```

Projects
--------

- List projects

    ```sh
    http get :8000/api/v1/projects/ "Authorization: JWT <token>"
    ```

- Create project

    ```sh
    http post :8000/api/v1/projects/ "Authorization: JWT <token>" name="New project"
    ```

- List project-related tasks

    ```sh
    http get :8000/api/v1/projects/1/tasks/ "Authorization: JWT <token>"
    ```

- Add task to project

    ```sh
    http post :8000/api/v1/projects/1/tasks/ \
        title="New issue" description="Harry up" due_date="2018-01-01" assignee_id=1 \
        "Authorization: JWT <token>"
    ```

- Remove task from project

    ```sh
    http delete :8000/api/v1/projects/1/tasks/29/ "Authorization: JWT <token>"
    ```

- Remove project

    ```sh
    http delete :8000/api/v1/projects/1/ "Authorization: JWT <token>"
    ```
