#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import (
    include, url
)
from django.contrib import admin
from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter
from rest_framework_jwt.views import obtain_jwt_token

from api.views import (
    UserResource, CurrentUserResource, UserList, TaskResource, TaskList,
    ProjectResource, ProjectList, NestedTaskResource, NestedTaskList,
)


router = routers.DefaultRouter()
router.register(r'users', UserList, base_name='user')
router.register(r'users', UserResource, base_name='user')
router.register(r'tasks', TaskList, base_name='task')
router.register(r'tasks', TaskResource, base_name='task')
router.register(r'projects', ProjectList, base_name='project')
router.register(r'projects', ProjectResource, base_name='project')

project_router = NestedSimpleRouter(router, r'projects', lookup='project')
project_router.register(r'tasks', NestedTaskList, base_name='project-task')
project_router.register(r'tasks', NestedTaskResource, base_name='project-task')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/users/me/$', CurrentUserResource.as_view({'get': 'retrieve'}), name='me'),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/', include(project_router.urls)),
    url(r'^api/v1/auth/jwt/$', obtain_jwt_token, name='obtain_jwt_token'),
    url(r'^api/v1/auth/', include('rest_framework.urls', namespace='rest_framework')),
]
